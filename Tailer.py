import sublime
import sublime_plugin
import os
import threading
import time


class TailCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        # settings
        settings = sublime.load_settings("Tailer.sublime-settings")
        path = settings.get('path')
        rows = settings.get('rows')

        # open new buffer
        view = self.view.window().new_file()
        view.set_name('Apache2 Error Log')
        view.set_scratch(True)
        view.set_syntax_file("Packages/tailer/Tailer.tmLanguage")
        view.insert(edit, view.size(), "Viewing " + rows + " lines from " + path + ".\n")

        #read x lines from the end
        file = open(path, 'r')

        try:
            rows = 0 - int(rows)
        except:
            rows = -10

        for line in file.readlines()[rows:]:
            view.insert(edit, view.size(), line)

        thread = TailThread(edit, view, path, rows)
        thread.start()


class TailThread(threading.Thread):

    def __init__(self, edit, view, path, rows):
        self.edit = edit
        self.view = view
        self.path = path
        self.rows = rows
        self.line = ""
        threading.Thread.__init__(self)

    def run(self):
        #seek to end of files
        file = open(self.path, 'r')
        st_results = os.stat(self.path)
        st_size = st_results[6]
        file.seek(st_size)

        #insert new lines to feed or sleep for a sec
        while True:
            where = file.tell()
            line = file.readline()
            if not line:
                time.sleep(1)
                file.seek(where)
            else:
                self.line = line

                def _insert(self, line):
                    self.view.insert(self.edit, self.view.size(), self.line)
                sublime.set_timeout(lambda: _insert(self, line), 100)
